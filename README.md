# A Slack web"app" for Ubuntu Touch

## Building

To build this for Ubuntu Touch, you need to have Clickable installed. Instruction on how to install it can be found on [Clickable homepage](http://clickable.bhdouglass.com/en/latest/).

Then just run the following command in the project root directory:

```
clickable
```

In order to launch this directly on a PC, try:

```
clickable desktop
```

## Debugging

To run and debug this directy on a phone/tablet via USB connection execute:

```
clickable launch && clickable logs
```

Or if you have SSH enabled on your device, you may want to run:

```
clickable --ssh IP_ADDRESSorHOSTNAME && clickable logs --ssh IP_ADDRESSorHOSTNAME
```
