import QtQuick 2.9
import QtQuick.Window 2.2
import QtWebEngine 1.5
import Morph.Web 0.1
import Ubuntu.Components 1.3 as UITK

Window {
    id: window
    width: 800
    height: 600
    // visibility: Window.Windowed
    title: "Slack web chat"
    
    property string dataPath: "/home/phablet/.config/slack-base-webapp-chat"
    property string pageUrl: "https://slack.com/messages"
    property bool hasLoggedIn: false
    
    Item {
        id: root
        anchors {
            fill: parent
            bottomMargin: UbuntuApplication.inputMethod.visible ? UbuntuApplication.inputMethod.keyboardRectangle.height : 0
            Behavior on bottomMargin {
                NumberAnimation {
                    duration: 175
                    easing.type: Easing.OutQuad
                }
            }
        }
        
        WebEngineView {
            id: webview
            anchors.fill: parent
            
            property string lastValidUrl: ""
            
            url: pageUrl
            settings.touchIconsEnabled: true
            
            // Pretend you are an up-to-date non-mobile browser for Slack to work correctly
            profile.httpUserAgent: "Mozilla/5.0 (X11; Linux; Ubuntu 16.04) AppleWebKit/537.36 (KHTML, like Gecko) snap Chromium/83.0.4086.0 Chrome/83.0.4086.0 Safari/537.36"
            profile.cachePath: dataPath
            profile.persistentStoragePath: dataPath
            
            onUrlChanged: {
                var _url = url.toString();
                var slackUrl = /https?:\/\/.*slack\.com\/.*/;
		var appUrl = /https?:\/\/app.slack.com\/client/;

                console.log("url is " + _url);

		if (!hasLoggedIn && appUrl.test(_url)) {
		    hasLoggedIn = true;
		}
                if(!slackUrl.test(_url) && hasLoggedIn) {
                    console.log("launching externally");
                    Qt.openUrlExternally(_url);
                    url = lastValidUrl;
                }
                else {
                    lastValidUrl = url;
                }
            }
            
            onNewViewRequested: function(request) {
                console.log("External URL requested, opening:", request.requestedUrl);
                console.log("userInitiated", request.userInitiated ? "true" : "false");
                Qt.openUrlExternally(request.requestedUrl);
            }
            zoomFactor: 2.12
        }
    }
}
